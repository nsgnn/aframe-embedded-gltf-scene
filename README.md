# [aframe-embedded-gltf-scene](https://camden-bock.gitlab.io/aframe-embedded-gltf-scene/)  [![pipeline status](https://gitlab.com/camden-bock/aframe-embedded-gltf-scene/badges/main/pipeline.svg)](https://gitlab.com/camden-bock/aframe-embedded-gltf-scene/-/commits/main)


This is an embeddable gltf sample viewer using an a-frame scene and a-frame asset management.  

Add the following to your ``head``
```html
    <script src="https://aframe.io/releases/1.0.4/aframe.min.js"></script>
    <script src="https://unpkg.com/aframe-orbit-controls@1.2.0/dist/aframe-orbit-controls.min.js"></script>
    <script src="https://unpkg.com/aframe-supercraft-loader@1.1.3/dist/aframe-supercraft-loader.js"></script>
```

If hosting through GitHub Pages add to ``Body``:
```html
    <!-- Geology Sample Viewer AFRAME -->
    <style>
      #geologySampleViewer {
        width:80%;
        height:80%;
      }
    </style>
    <div id="geologySampleViewer">
        <a-scene embedded transparent>
          <a-assets>
             <a-asset-item id="sample" src="assets/samplerock.gltf"></a-asset-item>
           </a-assets>
           <a-entity camera look-controls orbit-controls="target: 0 1.6 0; minDistance: 0.3; maxDistance: 180; initialPosition: 0 1.6 .4; enablePan: false"></a-entity>
           <!-- Using the asset management system.-->
          <a-gltf-model drag-rotate-component src="#sample" position="0 1.6 0"></a-gltf-model>
      </a-scene>
    </div>
    <!-- end AFRAME -->
```

If hosting through other service (e.g. Brightspace), add to ``Body``:
```html
    <!-- Geology Sample Viewer AFRAME -->
    <style>
      #geologySampleViewer {
        width:80%;
        height:80%;
      }
    </style>
    <div id="geologySampleViewer">
        <a-scene embedded transparent>
          <a-assets>
             <a-asset-item id="sample" src="https://rawcdn.githack.com/camden-bock/aframe-geology-sample-viewer/476892532fac364cde92d05f2f893859873b46d6/assets/samplerock.gltf"></a-asset-item>
           </a-assets>
           <a-entity camera look-controls orbit-controls="target: 0 1.6 0; minDistance: 0.3; maxDistance: 180; initialPosition: 0 1.6 .4; enablePan: false"></a-entity>
           <!-- Using the asset management system.-->
          <a-gltf-model drag-rotate-component src="#sample" position="0 1.6 0"></a-gltf-model>
      </a-scene>
    </div>
    <!-- end AFRAME -->
```

Compatability is confirmed with D2L Brightspace Course Managment - requires plaintext html editor (no WYSG)

## Dependencies:
 - Look-Controls
 - Orbit-Controls
 - GLTF-Viewer

## Still need Help?

### Installation

First make sure you have Node installed.

On Mac OS X, it's recommended to use [Homebrew](http://brew.sh/) to install Node + [npm](https://www.npmjs.com):

    brew install node

To install the Node dependencies:

    npm install


### Local Development

To serve the site from a simple Node development server:

    npm start

Then launch the site from your favourite browser:

[__http://localhost:3000/__](http://localhost:3000/)

If you wish to serve the site from a different port:

    PORT=8000 npm start


## License

This program is free software and is distributed under an [MIT License](LICENSE).
